#!/bin/bash

HELP="\n
Установка инструментов разработки:\n
\t $ tortilla ide\n
\n
Создание нового Django проекта:\n
\t $ tortilla new 'имя_проекта'\n
\n
Клонирование существующего Django проекта:\n
\t $ tortilla clone 'имя_проекта'\n
"
