#!/bin/bash

SOFT_IDE="
python3.*-dev
python3.*-venv
python-setuptools
gcc
git
dia
sqlitebrowser
zsh
inkscape
"

SOFT_SERVER="
python3.*-dev
python3.*-venv
python-setuptools
gcc
git
zsh
tree
terminator
nginx
"

venv_dir="/home/$USER/.virtenv"
bitbucket_user="mikhail_valkov"
email="mikhail.valkov@gmail.com"
django_version="1.11.1"

color_error='\033[0;31m'
color_done='\033[0;32m'
color_warn='\033[0;33m'
color_normal='\033[0m'
