#!/bin/bash

# переход в рабочий каталог скрипта
cd $(dirname $(readlink -e $0))

# подключаем модули
source "./settings.sh"
source "./lib.sh"
source "./help.sh"

case $1 in
    # установка инструментов разработки
    ide)
        echo -en $color_warn "*** Обновляю базу приложений\n"
        sudo apt update > /dev/null 2>&1
        for itm in $SOFT_IDE
        do
        setup_package $itm
        done
        create_dir $venv_dir

        git config --global user.name $bitbucket_user
        git config --global user.email $email
        
        exit 0;
    ;;

    server)
        echo -en $color_warn "*** Обновляю базу приложений\n"
        sudo apt update > /dev/null 2>&1
        for itm in $SOFT_SERVER
        do
        setup_package $itm
        done
        create_dir $venv_dir
        exit 0;
    ;;
    # создание нового проекта
    new)
        if [ -z "$2" ]
        then
            echo -en $color_warn $HELP
            exit 1
        else
            make_venv $2
            exit 0
        fi
        ;;
    # клонирование проекта
    clone)
        if [ -z "$2" ]
        then
            echo -en $color_warn $HELP
            exit 1
        else
            clone_project $2
            exit 0
        fi
        ;;
    #вывод справки по запросу, либо если не был введен ключ
    help|*)
        echo -en $color_warn $HELP
        exit 0
    ;;
esac
