#!/bin/bash

# функция установки пакетов
function setup_package {
  # This a bug with locale. If your locale is not EN, then bug.
  pac=`apt-cache policy $1 | grep "Installed: (none)"`
  if [ -n "$pac" ]
  then
    echo -en $color_error "***" $1 " не обнаружен. Устанавливаю.\n"
    sudo apt-get -y install $1 > /dev/null 2>&1
    echo -en $color_done "***" $1 " установлен.\n"
  else
    echo -en $color_done "***" $1 " обнаружен.\n"
  fi
}

# функция создания дирректории, если ее нет
function create_dir {
  if [ -e "$1" ]
  then
      echo -en $color_warn "***" $1 " Каталог уже существует.\n"
  else
      mkdir $1
      #sudo chown -R $USER $1
      cd $1
      echo -en $color_done "***" $1 " Каталог создан.\n"
  fi
}

# создаем виртуальное окружение
function make_venv {
    project_path="$venv_dir/$1"
    if [ -d $project_path ]
      then
        echo -en $color_error "*** $project_path уже существует!\n"
        exit 1
      else
        cd $venv_dir
        python3 -m venv $1
        echo -en $color_done "*** Виртуальное окружение" $project_path "создано.\n"
        source "$project_path/bin/activate"
        cd $project_path
        project_path="$venv_dir/$1"
        mkdir "$project_path/Ideas"
        ln -s "$project_path" "/home/$USER/$1.project"
        pip install django==$django_version > /dev/null 2>&1
        echo -en $color_done "*** Django установлен.\n"
        django-admin startproject $1
        echo -en $color_done "*** Проект Django: $1 создан.\n"
        pip install --upgrade pip > /dev/null 2>&1
        echo -en $color_done "*** Pip обновлен.\n"
        pip freeze > requirements.txt

        # TODO: move this code 
        #git config --global user.name $bitbucket_user
        #git config --global user.email $email

        gitignore_origin="$(dirname $(readlink -e $0))/shared/.gitignore"
        echo $gitignore_origin
        gitignore_target="$project_path/.gitignore"
        echo $gitignore_target
        cp $gitignore_origin $gitignore_target

        echo -en $color_warn "*** Создайте $1 на bitbucket.org\n"
        xdg-open "https://bitbucket.org/repo/create?owner=$bitbucket_user" > /dev/null 2>&1 &
        echo -en $color_warn "*** Чтобы продолжить, нажмите [ENTER]"
        read buff
        echo -en $color_warn "*** Синхронизирую проект с bitbucket.org\n"
        git init > /dev/null 2>&1
        touch "$project_path/Ideas/.for_git"
        git add Ideas $1 .gitignore requirements.txt > /dev/null 2>&1
        #git add .
        git commit -m "Чистый Django проект" > /dev/null 2>&1
        git remote add origin "ssh://git@bitbucket.org/$bitbucket_user/$1.git" > /dev/null 2>&1
        git push -u origin master > /dev/null 2>&1
        echo -en $color_done "*** Проект синхронизирован.\n"
    fi
    echo -en $color_done "*** Проект готов к работе\n"
}

# клонирует проект с bitbucket.org
function clone_project {
    project_path="$venv_dir/$1"
    if [ -d $project_path ]
      then
        echo -en $color_error "*** $project_path уже существует!\n"
        exit 1
      else
        mkdir $project_path
        cd $project_path
        echo -en $color_warn "*** "
        git clone "ssh://git@bitbucket.org/$bitbucket_user/$1.git" . > /dev/null 2>&1
        echo -en $color_done "*** $1 синхронизирован.\n"
        python3 -m venv $project_path
        echo -en $color_done "*** Виртуальное окружение" $project_path "создано.\n"
        source "$project_path/bin/activate"
        pip install --upgrade pip > /dev/null 2>&1
        echo -en $color_done "*** Pip обновлен.\n"
        pip install -r requirements.txt > /dev/null 2>&1
        echo -en $color_done "*** Зависимости установлены.\n"
        ln -s "$project_path" "/home/$USER/$1.project"
        echo -en $color_done "*** Проект готов к работе\n"
    fi
}
